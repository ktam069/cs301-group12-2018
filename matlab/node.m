classdef node
    properties
        position % vector for position
        f % g + h (movement parameter)
        parent
    end
    methods
        function [list] = add(list, node)
            list = [list, node];
        end

        function [list, node] = remove(list, index)
            node = list(index);
            length = size(list, 2);

            if (length == 1) 
                list = [];
            elseif (index == 1)
                list = list(2:end);
            elseif (index == length)
                list = list(1:end - 1);
            else
                list1 = list(1:index - 1);
                list2 = list(index + 1:end);

                list = [list1, list2];
            end
        end

        function [isEmpty] = isEmpty(list)
            isEmpty = false;
            length = size(list, 2);

            if (length == 0)
                isEmpty = true;
            end
        end    
    end
end