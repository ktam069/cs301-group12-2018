% use manhattan heuristic: 
% h = abs(current_cell.x - goal.x) + abs(current_cell.y - goal.y)
% used when only 4 directions of movement allowed
function [retmap,retsteps] = astar( mapfile,startlocation,targetlocation)
    openList = [];
    closedList = [];
    
    retmap = map_convert(mapfile); % convert map into matrix of ones and zeros
    [r,c] = size(retmap); % y, x
    
    retsteps = [];
%     steps = path;
    
    start = node;
    start.position = startlocation;
    start.f = 0;
    start.parent = 0; % matlab doesn't support NULL

    openList = add(openList, start);

    while (~isEmpty(openList))
        index = getIndexOfSmallestF(openList);
        [openList, nodeWithSmallestF] = remove(openList, index);


        % North
        if (nodeWithSmallestF.position(1) > 1 && retmap(nodeWithSmallestF.position(1) - 1, nodeWithSmallestF.position(2)) == 0)
            north = node;
            north.position(1) = nodeWithSmallestF.position(1) - 1;
            north.position(2) = nodeWithSmallestF.position(2);

            if(isequal(north.position, targetlocation))
                retsteps = [retsteps; nodeWithSmallestF.position; north.position];
%                 steps = addStep(steps, nodeWithSmallestF.position, north.position);
%                 retsteps = getFinalPath(steps);
                return
            else
                north.f = calculateH(north.position, targetlocation) + 1;
                if (~hasNodeWithSamePositionAndLowerF(north, openList) && ~hasNodeWithSamePositionAndLowerF(north, closedList))
                    openList = add(openList, north);
                end
            end
        end
        % East
        if (nodeWithSmallestF.position(2) < c && retmap(nodeWithSmallestF.position(1), nodeWithSmallestF.position(2) + 1) == 0)
            east = node;
            east.position(1) = nodeWithSmallestF.position(1);
            east.position(2) = nodeWithSmallestF.position(2) + 1;

            if(isequal(east.position, targetlocation))
                retsteps = [retsteps; nodeWithSmallestF.position; east.position];
%                 steps = addStep(steps, nodeWithSmallestF.position, east.position);
%                 retsteps = getFinalPath(steps);
                return
            else
                east.f = calculateH(east.position, targetlocation) + 1;
                if (~hasNodeWithSamePositionAndLowerF(east, openList) && ~hasNodeWithSamePositionAndLowerF(east, closedList))
                    openList = add(openList, east);
                end
            end
        end
        % South
        if (nodeWithSmallestF.position(1) < r && retmap(nodeWithSmallestF.position(1) + 1, nodeWithSmallestF.position(2)) == 0)
            south = node;
            south.position(1) = nodeWithSmallestF.position(1) + 1;
            south.position(2) = nodeWithSmallestF.position(2);

            if(isequal(south.position, targetlocation))
                retsteps = [retsteps; nodeWithSmallestF.position; south.position];
%                 steps = addStep(steps, nodeWithSmallestF.position, south.position);
%                 retsteps = getFinalPath(steps);
                return
            else
                south.f = calculateH(south.position, targetlocation) + 1;
                if (~hasNodeWithSamePositionAndLowerF(south, openList) && ~hasNodeWithSamePositionAndLowerF(south, closedList))
                    openList = add(openList, south);
                end
            end
        end
        % West
        if (nodeWithSmallestF.position(2) > 1 && retmap(nodeWithSmallestF.position(1), nodeWithSmallestF.position(2) - 1) == 0)
            west = node;
            west.position(1) = nodeWithSmallestF.position(1);
            west.position(2) = nodeWithSmallestF.position(2) - 1;

            if(isequal(west.position, targetlocation))
                retsteps = [retsteps; nodeWithSmallestF.position; west.position];
%                 steps = addStep(steps, nodeWithSmallestF.position, west.position);
%                 retsteps = getFinalPath(steps);
                return
            else
                west.f = calculateH(west.position, targetlocation) + 1;
                if (~hasNodeWithSamePositionAndLowerF(west, openList) && ~hasNodeWithSamePositionAndLowerF(west, closedList))
                    openList = add(openList, west);
                end
            end
        end

        closedList = add(closedList, nodeWithSmallestF);
%         steps = addStep(steps, nodeWithSmallestF.position);
%         retsteps = getFinalPath(steps);
        retsteps = [retsteps; nodeWithSmallestF.position];
    end
    
end

function [hasSamePositionAndLowerF] = hasNodeWithSamePositionAndLowerF ( node, list )
    hasSamePositionAndLowerF = false;

    for i = 1:size(list,2)
        if (isequal(node.position, list(i).position) && list(i).f <= node.f)
            hasSamePositionAndLowerF = true;
        end
    end
end

function [indexOfSmallestF] = getIndexOfSmallestF( list )
    indexOfSmallestF = 1;
    length = size(list, 2);

    if (length > 1)
        for i = 2:length
            if (list(i).f < list(indexOfSmallestF).f)
                indexOfSmallestF = i;
            end
        end
    end
end

function [h] = calculateH (currentlocation, targetlocation)
    h = abs(currentlocation(1) - targetlocation(1)) + abs(currentlocation(2) - targetlocation(2));
end