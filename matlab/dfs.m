%% This is a shell that you will have to follow strictly. 
% You will use the plotmap() and viewmap() to display the outcome of your algorithm.

% Load sample_data_map_8, three variables will be created in your workspace. These were created as a 
% result of [m,v,s]=dfs('map_8.txt',[14,1],[1,18]);
% The solution can be viewed using 
% plotmap(m,s) 

% write your own function for the DFS algorithm.
% there will be an error if startlocation and targetlocation are the same position due to plotmap function provided
function [retmap,retvisited,retsteps] = dfs( mapfile,startlocation,targetlocation)
    retmap = map_convert(mapfile); % convert map into matrix of ones and zeros
    [r,c] = size(retmap);
    
    rows = r; % rows and cols of matrix showing visited and unvisited cells
    cols = c;
    retvisited = ones(rows, cols);
    retvisited(startlocation(1), startlocation(2)) = 0; % set starting position as visited
    disp(retvisited);
    
    retsteps = startlocation;
    [retvisited, retsteps, ~] = dfsRecursive(retmap, retvisited, retsteps, startlocation, targetlocation, false);
end

function [retvisited, retsteps, targetFound] = dfsRecursive(retmap, retvisited, retsteps, currentPosition, targetlocation, targetFound)
    if(isequal(currentPosition, targetlocation))
        targetFound = true; % target is found
    end

    if(targetFound == false) 
        retsteps = [retsteps; currentPosition];
        retvisited(currentPosition(1), currentPosition(2)) = 0;
    else
        return;
    end

    [m,n] = size(retmap);

    if(currentPosition(2) > 1 && retmap(currentPosition(1), currentPosition(2) - 1) == 0 && retvisited(currentPosition(1), currentPosition(2) - 1) == 1)
        % North tile
        newTile = [currentPosition(1), currentPosition(2) - 1];
        [retvisited, retsteps, targetFound] = dfsRecursive(retmap, retvisited, retsteps, newTile, targetlocation, targetFound);
    end
    if(currentPosition(1) < m && retmap(currentPosition(1) + 1, currentPosition(2)) == 0 && retvisited(currentPosition(1) + 1, currentPosition(2)) == 1)
        % East tile
        newTile = [currentPosition(1) + 1, currentPosition(2)];
        [retvisited, retsteps, targetFound] = dfsRecursive(retmap, retvisited, retsteps, newTile, targetlocation, targetFound);
    end
    if(currentPosition(2) < n && retmap(currentPosition(1), currentPosition(2) + 1) == 0 && retvisited(currentPosition(1), currentPosition(2) + 1) == 1)
        % South tile
        newTile = [currentPosition(1), currentPosition(2) + 1];
        [retvisited, retsteps, targetFound] = dfsRecursive(retmap, retvisited, retsteps, newTile, targetlocation, targetFound);
    end
    if(currentPosition(1) > 1 && retmap(currentPosition(1) - 1, currentPosition(2)) == 0 && retvisited(currentPosition(1) - 1, currentPosition(2)) == 1)
        % West tile
        newTile = [currentPosition(1) - 1, currentPosition(2)];
        [retvisited, retsteps, targetFound] = dfsRecursive(retmap, retvisited, retsteps, newTile, targetlocation, targetFound);
    end
end

function placestep(position,i)
% This function will plot a insert yellow rectangle and also print a number in this rectangle. Use with plotmap/viewmap. 
position = [16-position(1) position(2)];
position=[position(2)+0.1 position(1)+0.1];
rectangle('Position',[position,0.8,0.8],'FaceColor','y');
c=sprintf('%d',i);
text(position(1)+0.2,position(2)+0.2,c,'FontSize',10);
end
