#include <stdio.h>
#include <stdlib.h>

#include "linked_list.c"
#include "map.h"		// insert the map to be loaded here

#define MAP_HEIGHT 15				// dimensions of the map
#define MAP_WIDTH  19
#define MAX_PATH_LENGTH 150			// (some arbitrarily large number, for the longest expected path required)

typedef struct Pair {
	int row;
	int col;
} Pair;

Pair aStarPath[MAX_PATH_LENGTH] = {{-1, -1}};		// stores every grid of the a*; note that the path continues after reaching the last node (until it returns to the start)

node* astar(int startRow, int startCol, int targetRow, int targetCol);
int calculateH(int currentRow, int currentCol, int targetRow, int targetCol);
bool hasNodeWithSamePositionAndLowerF(node n, node* head);
void convertPathFromLinkedListToArray(node* head);

node* astar(int startRow, int startCol, int targetRow, int targetCol) {
    node* open = NULL;
    node* closed = NULL;
    node* path = NULL;
    node* nodeWithSmallestF = NULL;

    node* startLocation = create(startRow, startCol, 0, -1, NULL);
    open = append(open, startLocation);

    while (!isEmpty(open)) {
        nodeWithSmallestF = get_node_with_smallest_f(open);
        open = remove_any(open, nodeWithSmallestF);

        // North
        if (nodeWithSmallestF.row > 1 && map[nodeWithSmallestF.row - 1][nodeWithSmallestF.col] == 0) {
            int f = calculateH(nodeWithSmallestF.row - 1, nodeWithSmallestF.col, targetRow, targetCol) + 1;
            node* north = create(nodeWithSmallestF.row - 1, nodeWithSmallestF.col, f, -1, NULL);

            if (north.row == targetRow && north.col == targetCol) {
                path = append(path, nodeWithSmallestF, north);
                return path;
            } else if (!hasNodeWithSamePositionAndLowerF(open, north) && !hasNodeWithSamePositionAndLowerF(closed, north)) {
                open = append(open, north);
            }
        }

        // East
        if (nodeWithSmallestF.col < MAP_WIDTH && map[nodeWithSmallestF.row][nodeWithSmallestF.col + 1] == 0) {
            int f = calculateH(nodeWithSmallestF.row, nodeWithSmallestF.col + 1, targetRow, targetCol) + 1;
            node* east = create(nodeWithSmallestF.row, nodeWithSmallestF.col + 1, f, -1, NULL);

            if (east.row == targetRow && east.col == targetCol) {
                path = append(path, nodeWithSmallestF, east);
                return path;
            } else if (!hasNodeWithSamePositionAndLowerF(open, east) && !hasNodeWithSamePositionAndLowerF(closed, east)) {
                open = append(open, east);
            }
        }

        // South
        if (nodeWithSmallestF.row < MAP_HEIGHT && map[nodeWithSmallestF.row + 1][nodeWithSmallestF.col] == 0) {
            int f = calculateH(nodeWithSmallestF.row + 1, nodeWithSmallestF.col, targetRow, targetCol) + 1;
            node* south = create(nodeWithSmallestF.row + 1, nodeWithSmallestF.col, f, -1, NULL);

            if (south.row == targetRow && south.col == targetCol) {
                path = append(path, nodeWithSmallestF, south);
                return path;
            } else if (!hasNodeWithSamePositionAndLowerF(open, south) && !hasNodeWithSamePositionAndLowerF(closed, south)) {
                open = append(open, south);
            }
        }

        // West
        if (nodeWithSmallestF.col > 1 && map[nodeWithSmallestF.row][nodeWithSmallestF.col - 1] == 0) {
            int f = calculateH(nodeWithSmallestF.row, nodeWithSmallestF.col - 1, targetRow, targetCol) + 1;
            node* west = create(nodeWithSmallestF.row, nodeWithSmallestF.col - 1, f, -1, NULL);

            if (west.row == targetRow && west.col == targetCol) {
                path = append(path, nodeWithSmallestF, west);
                return path;
            } else if (!hasNodeWithSamePositionAndLowerF(open, west) && !hasNodeWithSamePositionAndLowerF(closed, west)) {
                open = append(open, west);
            }
        }
    }

    closed = append(closed, nodeWithSmallestF);
    path = append(path, nodeWithSmallestF);
}

void convertPathFromLinkedListToArray(node* head) {
    node* cursor = head;
    int i = 0;

    while (cursor != NULL) {
        aStarPath[i].row = cursor.row;
        aStarPath[i].col = cursor.col;
        i++;

        cursor = cursor->next;
    }
	
    //free(cursor);
}

int calculateH(int currentRow, int currentCol, int targetRow, int targetCol) {
    return abs(currentRow - targetRow) + abs(currentCol - targetCol);
}

bool hasNodeWithSamePositionAndLowerF(node* head, node n) {
    node* cursor = head;
    while (cursor != NULL) {
        if (n.row == cursor.row && n.col == cursor.col && cursor.f <= n.f) {
            return true;
        }
        cursor = cursor->next;
    }

    return false;
}
