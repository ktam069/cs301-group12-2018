#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 64 // USBUART fixed buffer size
#define PACKETSIZE 32

#define CHAR_NULL '0'
#define CHAR_BACKSP 0x08
#define CHAR_DEL 0x7F
#define CHAR_ENTER 0x0D
#define LOW_DIGIT '0'
#define HIGH_DIGIT '9'
#define SOP 0xaa

#define CONVERT_TO_ASCII    (0x30u)

char rf_string[RXSTRINGSIZE];
char line[BUF_SIZE], entry[BUF_SIZE];
uint8 usbBuffer[BUF_SIZE];

void usbPutString(char *s);
void usbPutChar(char c);
// void handle_rx_binary();
// char handle_rx_ascii(uint8 value);
void handle_usb();
// void handle_rf();

void usbPutString(char *s){
    // !! Assumes that *s is a string with allocated space >=64 chars     
    //  Since USB implementation retricts data packets to 64 chars, this function truncates the
    //  length to 62 char (63rd char is a '!')

    #ifdef USE_USB     
        while (USBUART_CDCIsReady() == 0);
        s[63]='\0';
        s[62]='!';
        USBUART_PutData((uint8*)s,strlen(s));
    #endif
}

void usbPutChar(char c){
    #ifdef USE_USB
        while (USBUART_CDCIsReady() == 0);
        USBUART_PutChar(c);
    #endif    
}

void handle_usb(){
    // handles input at terminal, echos it back to the terminal
    // turn echo OFF, key emulation: only CR
    // entered string is made available in 'line' and 'flag_KB_string' is set
    
    static uint8 usbStarted = FALSE;
    static uint16 usbBufCount = 0;
    uint8 c; 
    

    if (!usbStarted) {
        if (USBUART_GetConfiguration()) {
            USBUART_CDC_Init();
            usbStarted = TRUE;
        }
    }
    else {
        if (USBUART_DataIsReady() != 0) {  
            c = USBUART_GetChar();

            if ((c == 13) || (c == 10)) {
//                if (usbBufCount > 0) {
                    entry[usbBufCount]= '\0';
                    strcpy(line,entry);
                    usbBufCount = 0;
                    flag_KB_string = 1;
//                }
            }
            else {
                if (((c == CHAR_BACKSP) || (c == CHAR_DEL) ) && (usbBufCount > 0) ) {
                    usbBufCount--;
                } else {
                    if (usbBufCount > (BUF_SIZE-2) ) { // one less else strtok triggers a crash
                       USBUART_PutChar('!');        
                    }
                    else {
                        entry[usbBufCount++] = c;  
                    }
                }  
            }
        }
    }    
}