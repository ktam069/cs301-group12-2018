#include <stdio.h>
#include <stdlib.h>

#define TARGET_IN_MM 1000
#define TARGET_PERCENT_SPEED 0.25

#define DEFAULT_SPEED 175			// robot speed

uint8 defaultWheelSpeed = DEFAULT_SPEED;
uint8 stationaryWheelSpeed = 127;

uint8 turningInPlaceForwardWheelSpeed = 169;
uint8 turningInPlaceReverseWheelSpeed = 86;

uint8 going_straight = 0;

volatile uint16 targetCount = (uint16) (TARGET_IN_MM/0.95);
volatile uint16 decelCounts = 300;

void turn_left();
void turn_right();
void turn_left_in_place();
void turn_right_in_place();
void go_straight();
void straight_no_quad();

void speed_up_right();
void speed_up_left();
void slow_down_right();
void slow_down_left();
void speed_up_both(uint8 step_size);
void slow_down_both(uint8 step_size);
void stop();

void slight_left();
void slight_right();

void turn_left(){
    PWM_1_WriteCompare(defaultWheelSpeed-18);			// (changed from turning on one wheel to smoother turning)
    PWM_2_WriteCompare(defaultWheelSpeed);
    
    going_straight = 0;
}

void turn_right(){
    PWM_1_WriteCompare(defaultWheelSpeed);
    PWM_2_WriteCompare(defaultWheelSpeed-18);
    
    going_straight = 0;
}

void turn_left_in_place(){
    PWM_1_WriteCompare(turningInPlaceReverseWheelSpeed);
    PWM_2_WriteCompare(turningInPlaceForwardWheelSpeed);
    
    going_straight = 0;
}

void turn_right_in_place(){
    PWM_1_WriteCompare(turningInPlaceForwardWheelSpeed);
    PWM_2_WriteCompare(turningInPlaceReverseWheelSpeed-5);		// (a slight hard-coded offset to compensate for the difference in default wheel speeds)
    
    going_straight = 0;
}

void go_straight(){
	PWM_1_WriteCompare(defaultWheelSpeed);
	PWM_2_WriteCompare(defaultWheelSpeed);
    
    going_straight = 1;
}

void straight_no_quad() {
	PWM_1_WriteCompare(defaultWheelSpeed);
	PWM_2_WriteCompare(defaultWheelSpeed);
    
    going_straight = 0;
}

void speed_up_left() {
   PWM_1_WriteCompare(PWM_1_ReadCompare()+1);
}

void speed_up_right(){
   PWM_2_WriteCompare(PWM_2_ReadCompare()+1);
}

void slow_down_left() {
   PWM_1_WriteCompare(PWM_1_ReadCompare()-1);
}

void slow_down_right() {
   PWM_2_WriteCompare(PWM_2_ReadCompare()-1);
}

void speed_up_both(uint8 step_size){
    uint16 left_PWM  = PWM_1_ReadCompare();
    uint16 right_PWM = PWM_2_ReadCompare();
    
    PWM_1_WriteCompare(left_PWM  + step_size);
    PWM_2_WriteCompare(right_PWM + step_size);
}

void slow_down_both(uint8 step_size) {
    uint16 left_PWM  = PWM_1_ReadCompare();
    uint16 right_PWM = PWM_2_ReadCompare();
    
    if (left_PWM  - step_size > 165) {          // only decrease speed down to the stationary PWM value (i.e. don't move backwards)
        PWM_1_WriteCompare(left_PWM  - step_size);
        PWM_2_WriteCompare(right_PWM - step_size);
    } else {
        PWM_1_WriteCompare(165);
        PWM_2_WriteCompare(165);
    }
}

void stop() {
    PWM_1_WriteCompare(stationaryWheelSpeed);
    PWM_2_WriteCompare(stationaryWheelSpeed);
	
	going_straight = 0;
}

void slight_left() {
    PWM_2_WriteCompare(PWM_2_ReadCompare() + 1);
}

void slight_right() {
    PWM_1_WriteCompare(PWM_1_ReadCompare() + 1);   
}