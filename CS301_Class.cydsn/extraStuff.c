
void check_distance();
void check_decelerate();
uint16 calculate_constant_speed();

void check_distance() {                                       // stops if the distance travelled exceeds the desired distance
    if (distance_count >= targetCount) {
        stop();
    }
}


uint16 calculate_constant_speed() {         // converts a (linear) percentage speed into its equivalent PWM value
    double percentage_speed = (2*200)/255.0 - 1;              // default speed is 200 locked-antiphase
    double new_percentage_speed = 1 * percentage_speed;
    
    double new_PWM_value_double = 255 * (new_percentage_speed+1)/2.0;
    uint16 new_PWM_value = (uint16) (new_PWM_value_double + 0.5);
    
    return new_PWM_value;
}

void check_decelerate() {
    if (distance_count >= targetCount - decelCounts) {
        slow_down_both(8);
    }
}


void handle_rf(){
    if (flag_rx == 1) {
        char c = UART_GetChar();
        
        if (c == '#' || c == SOP) {      // end of previous transmission (/start of a new one)
            if (count >= 32) {
                usbPutString(rf_string);
                
                system_state = *(vtype1*)rf_string;         // cast data to struct

                if (system_state.rssi != 0) {
                    char line1[30];
                    sprintf(line1, "RSSI: %d \tIndex: %d \tGhost x pos: %d \n", system_state.rssi, system_state.index, system_state.g0_xpos);
                    usbPutString(line1);
                }
            }    
            count = 0;
        }
        else{
            rf_string[count] = c;
            count++;          
        }
        flag_rx = 0;
    }
}

CY_ISR(ADC_ISR){    // ADC ISR
    /**********************************************/
    /* Place user ADC ISR code here. */
    /* This can be a good place to place code */
    /* that is used to switch the input to the */
    /* ADC. It may be good practice to first */
    /* stop the ADC before switching the input */
    /* then restart the ADC. */
    /**********************************************/
    /* `#START MAIN_ADC_ISR` */
    //result = ADC_GetResult16(0);
    dataReady = 1;
    /* `#END` */
}
void handle_adc(){
    //int16 newReading = 0; 
    int32 mVolts  = 0; 
    int32 previousValue = 0;
    int16 adc_count = 0; 
    
    if (flag_KB_string == 1) {
        usbPutString(line);
        flag_KB_string = 0;
    }        
    
    if (dataReady != 0) { 
        dataReady = 0;
        /* Convert the ADC counts to mVolts. ADC is configured to be single channel */
        adc_count = ADC_GetResult16(0);
        result = ADC_CountsTo_mVolts(ADC_GetResult16(0));
        //newReading = result;
    /* More user code */
    } 
    
    /* If the result changes, send it to UART */
    if (previousValue != result) {    
        SendChannelVoltage(result, adc_count);
        previousValue = mVolts;
    }
}

static void SendChannelVoltage(int32 mVolts, int16 count){
    /* Send Volts to UART */
    usbPutChar((count/1000) + CONVERT_TO_ASCII);
    count %= 1000;
    usbPutChar((count/100) + CONVERT_TO_ASCII);
    count %= 100;
    usbPutChar((count/10) + CONVERT_TO_ASCII);
    count %= 10;
    usbPutChar(count + CONVERT_TO_ASCII);
    usbPutString(" mV\n");
    CyDelay(100);
    
    /* Send Volts to UART */
    /*usbPutChar((mVolts/1000) + CONVERT_TO_ASCII);
    mVolts %= 1000;
    usbPutChar((mVolts/100) + CONVERT_TO_ASCII);
    mVolts %= 100;
    usbPutChar((mVolts/10) + CONVERT_TO_ASCII);
    mVolts %= 10;
    usbPutChar(mVolts + CONVERT_TO_ASCII);
    usbPutString(" mV\n");
    CyDelay(100);*/
}

// -- ISR Stufffff --

CY_ISR_PROTO(myisr);

CY_ISR(myisr){
    flag_rx = 1;
}

CY_ISR(quaddec){                        // Quadrature Decoder ISR checks the counter every 1 second
    if (mode != 3) { return; }      // only run this ISR if the mode is 3
    
    M2_counter = QuadDec_M2_GetCounter();
    M1_counter = QuadDec_M1_GetCounter();
    
    distance_count += M2_counter;
    robot_speed = (uint16) (M2_counter / 0.5);          // where 0.5 is the timer period
    
    QuadDec_M1_SetCounter(0);
    QuadDec_M2_SetCounter(0);
    
    //if (going_straight == 1) {
        if (M1_counter < M2_counter) {
            speed_up_left();
            slow_down_right();
        } else if (M2_counter < M1_counter) {
            speed_up_right();
            slow_down_left();
        }
        
    //}
    
    //check_decelerate();
    //check_distance();
    
    Timer_TS_ReadStatusRegister();
}

CY_ISR(sensor1On) {             // front left sensor
    senseFlags[0] = 1;
    if (senseFlags[1]) {
        if (mode == 3) { return; }      // don't do this in mode 3
        go_straight();
    }
} 

CY_ISR(sensor1Off) {
    senseFlags[0] = 0;
    if (!senseFlags[1]) {
        if (mode == 3) { return; }
        go_straight();
    } else {
        if (mode == 0 || mode == 2) {
            turn_right();
        } else if (mode == 1) {
            turn_right_in_place();
        }
    }
} 

CY_ISR(sensor2On) {             // front right sensor
    senseFlags[1] = 1;
    if (senseFlags[0]) {
        go_straight();
    }
} 

CY_ISR(sensor2Off) {
    senseFlags[1] = 0;
    if (!senseFlags[0]) {
        go_straight();
    } else {
        if (mode == 0 || mode == 2) {
            turn_left();
        } else if (mode == 1) {
            turn_left_in_place();
        }
    }
} 

CY_ISR(sensor3On) {             // center left sensor
    senseFlags[2] = 1;
    if (mode == 2) {
        turn_left_in_place();    
    } else if (mode == 3) {
        slight_left();
    }
} 

CY_ISR(sensor3Off) {
    senseFlags[2] = 0;
} 

CY_ISR(sensor4On) {             // center ref sensor
    senseFlags[3] = 1;
} 

CY_ISR(sensor4Off) {
    senseFlags[3] = 0;
} 

CY_ISR(sensor5On) {             // center right sensor
    senseFlags[4] = 1;
    if (mode == 2) {
        turn_right_in_place();
    } else if (mode == 3) {
        slight_right();
    }
} 

CY_ISR(sensor5Off) {
    senseFlags[4] = 0;    
} 

// CY_ISR(switch1On) {           // bit 1 of mode
//     modeFlags[1] = 1;
//     mode = modeFlags[1]*2 + modeFlags[0];
// }

// CY_ISR(switch1Off) {
//     modeFlags[1] = 0;
//     mode = modeFlags[1]*2 + modeFlags[0];
// }

// CY_ISR(switch2On) {             // bit 0 of mode
//     modeFlags[0] = 1;
//     mode = modeFlags[1]*2 + modeFlags[0];
// }

// CY_ISR(switch2Off) {
//     modeFlags[0] = 0;
//     mode = modeFlags[1]*2 + modeFlags[0];
// }