#include "searches.c"

//=========================== Variables ===============================

enum dir {NORTH, EAST, SOUTH, WEST, NA};			// absolute direction that the robot is moving in (NA being not applicable)
enum dir direction = NA;

node pathVertices[MAX_PATH_LENGTH/2] = {{-1, -1, -1, 4, NULL}};
int pathIndex = 0;

int finalShortestPathIndex;				// index of target location in the pathVertices
enum dir lastDirection = NA;

//=====================================================================

void arrayToTurns(Pair arrayPath[MAX_PATH_LENGTH]);
enum dir findDirection(Pair currentLoc, Pair nextLoc);

uint8 isIntersection(int row, int col, enum dir d);
uint8 isDeadEnd(int row, int col, enum dir d);

void printPathVertices();

void shortenAStarPath();
void shiftArrayEntries(int i, int newI, int lastIndex);
int manhattanDistance(int currentRow, int currentCol, int targetRow, int targetCol);
int getLastArrayIndex();

//========================== Program Code =============================

void arrayToTurns(Pair arrayPath[MAX_PATH_LENGTH]){				// creates a linked list of vertices from the path of grid squares
	// Use the first two nodes to figure out the initial orientation of the robot
	direction = findDirection(arrayPath[0], arrayPath[1]);
	
	char line1[30];
	// sprintf(line1, "\n\nStarting conversion: \n___________\n");
	// if (DEBUG_MODE) usbPutString(line1);
	
	pathIndex = 0;
	
	int i;
	for (i = 1; i < MAX_PATH_LENGTH-1; i++){		// start from i = 1 as the first node was already checked; checks are done with (i) and (i+1) nodes
		// sprintf(line1, "\ni: %d  \n", i);
		// if (DEBUG_MODE) usbPutString(line1);
		enum dir nextDirection = findDirection(arrayPath[i], arrayPath[i+1]);
		
		int row = arrayPath[i].row;
		int col = arrayPath[i].col;
		
		/* NOTE: running this sprintf can change the values of row and column */
		//if (DEBUG_MODE) sprintf(line1, "\n   conv %d: \t(%d,%d) dir: %d -> %d", i, col, row, direction, nextDirection);
		//if (DEBUG_MODE) usbPutString(line1);
		
		if (row==endY && col==endX) {			//the last turn made was the final turn needed
			finalShortestPathIndex = pathIndex-1;
			lastDirection = direction;
		}
	
		// char line11[30];
		// sprintf(line11, "\nshortest path index: %d  \n", finalShortestPathIndex);
		// if (DEBUG_MODE) usbPutString(line11);
		
		// if (arrayPath[i+1].row == -1) return;
		if (arrayPath[i+1].row == -1) {
			// sprintf(line1, "\nContinue arrayToTurns");
			// if (DEBUG_MODE) usbPutString(line1);

			// return;		// reached the end of the path stored
			return;
		}
		if (direction == NA || nextDirection == NA) continue;		// error checking
		
		// Compare direction with nextDirection to figure out how the robot should turn at the node (vertex)
		if (nextDirection == (direction + 1) % 4) {		// turn right - north to east, east to south, etc.
			pathVertices[pathIndex++] = (node) {row, col, -1, RIGHT, NULL};

			sprintf(line1, "\nTURN RIGHT");
			if (DEBUG_MODE) usbPutString(line1);
		} else if (nextDirection == (direction + 2) % 4) {		// turn back - north to south, etc.
			if (isDeadEnd(row, col, direction)) {					// add BACK only if it's not a dead end
				pathVertices[pathIndex++] = (node) {row, col, -1, BACK, NULL};
			
				sprintf(line1, "\nTURN BACK");
				if (DEBUG_MODE) usbPutString(line1);
			}
		} else if (nextDirection == (direction + 3) % 4) {		// turn left - north to west, etc.
			pathVertices[pathIndex++] = (node) {row, col, -1, LEFT, NULL};

			sprintf(line1, "\nTURN LEFT");
			if (DEBUG_MODE) usbPutString(line1);
		} else if (nextDirection == (direction + 0) % 4){		// going straight - north to north, etc.
			if (isIntersection(row, col, direction)) {			// add STRAIGHT only if there is an intersection there
				pathVertices[pathIndex++] = (node) {row, col, -1, STRAIGHT, NULL};
				
				sprintf(line1, "\nGO STRAIGHT");
				if (DEBUG_MODE) usbPutString(line1);
			}
		}

		direction = nextDirection;
	}
	// sprintf(line1, "\n___________\n");
	// if (DEBUG_MODE) usbPutString(line1);
	
	// if (DEBUG_MODE) printPathVertices();
}

enum dir findDirection(Pair currentLoc, Pair nextLoc){		// returns the absolute direction, found using the locations of the two input nodes
	// if (currentLoc.row == -1 || nextLoc.row == -1) return NA;
	
	int x0 = currentLoc.col;
	int y0 = currentLoc.row;
	int x1 = nextLoc.col;
	int y1 = nextLoc.row;
	
	int deltaX = x1-x0;
	int deltaY = y1-y0;
	
	if 		(deltaX > 0) { return EAST; }		// next node is to the right of the current node
	else if (deltaX < 0) { return WEST; }		// next node is to the left of the current node
	else if (deltaY > 0) { return SOUTH; }		// next node is downwards from the current node
	else if (deltaY < 0) { return NORTH; }		// next node is upwards from the current node
	else { return NA; }							// (error case)
}

uint8 isIntersection(int row, int col, enum dir d){		// returns whether or not the current node is an intersection (when going straight)
	if (row==0 || col==0 || row==14 || col==18) { return 0; }
	if (d == NORTH || d == SOUTH) {		// check the nodes to the EAST and WEST
		return (map[row][col-1]==0 || map[row][col+1]==0);
	} else {		// if (d == EAST || d == WEST) check the nodes to the NORTH and SOUTH
		return (map[row-1][col]==0 || map[row+1][col]==0);
	}
}

uint8 isDeadEnd(int row, int col, enum dir d){
	if (row==0 || col==0 || row==14 || col==18) { return 0; }
	if 		(d == NORTH) { return (map[row-1][col]==0); }
	else if (d == SOUTH) { return (map[row+1][col]==0); }
	else if (d == EAST)  { return (map[row][col+1]==0); }
	else				 { return (map[row][col-1]==0); }
}

void printPathVertices(){
    int i = 0;
	char line1[30];
	sprintf(line1, "\n\n Vertices: \n___________\n");
	if (DEBUG_MODE) usbPutString(line1);
    for (i = 0; i < MAX_PATH_LENGTH/2-1; i++){
		//if (pathVertices[i].row == -1) break;
    	sprintf(line1, "\n Path index %d:\t(%d,%d) %d", i, pathVertices[i].row, pathVertices[i].col, pathVertices[i].turn);
    	if (DEBUG_MODE) usbPutString(line1);
	}
	sprintf(line1, "\n___________\n");
	if (DEBUG_MODE) usbPutString(line1);
}

// ---------------------

void shortenAStarPath(){
	int lastIndex = getLastArrayIndex();
	int i, j;
	for (i = 0; i <= lastIndex; i++) {
		for (j = lastIndex; j > i; j--) {
			Pair currentNode = aStarPathUsingArrays[i];
			Pair otherNode = aStarPathUsingArrays[j];
			uint8 dist = manhattanDistance(currentNode.row, currentNode.col, otherNode.row, otherNode.col);
			if (dist == 1 && j > i+1) {				// they are neighbouring nodes
				shiftArrayEntries(i+1, j, lastIndex);
			}
		}
	}
	
}

void shiftArrayEntries(int index, int newI, int lastIndex){		// move all entries from newI onwards down to i
	uint8 noToShift = newI - index;
	int i;
	for (i = index; i <= lastIndex; i++) {
		aStarPathUsingArrays[i] = aStarPathUsingArrays[i+noToShift];
	}
}

int manhattanDistance(int currentRow, int currentCol, int targetRow, int targetCol) {
    return abs(currentRow - targetRow) + abs(currentCol - targetCol);
}

int getLastArrayIndex(){
	int i = MAX_PATH_LENGTH-1;
	while (aStarPathUsingArrays[i].row == -1) {
		i--;
	}
	//char line1[30];
	//sprintf(line1, "\n last index is: %d \n", i);
	//if (DEBUG_MODE) usbPutString(line1);
	return i;
}

