//========================= DFS Program ===============================
//=====================================================================

#include <stdio.h>
#include <stdlib.h>

#include "map.h"		// insert the map to be loaded here

//========================== Variables ================================

#define MAP_HEIGHT 15				// dimensions of the map
#define MAP_WIDTH  19
#define MAX_PATH_LENGTH 150			// (some arbitrarily large number, for the longest expected path required)

typedef struct Pair {
	int row;
	int col;
} Pair;

Pair DFSPath[MAX_PATH_LENGTH] = {{-1, -1}};		// stores every grid of the DFS; note that the path continues after reaching the last node (until it returns to the start)
int DFSLevel = 0;					// keeps track of the node traversal order (which is used as the index for the DSFPath array)

// -- Settings --
int startX=1, startY=1;				// the robot's starting location

//int map[MAP_HEIGHT][MAP_WIDTH] = {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
//								  {1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1},
//								  {1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1},
//								  {1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1},
//								  {1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1},
//								  {1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1},
//								  {1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1},
//								  {1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1},
//								  {1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1},
//								  {1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1},
//								  {1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
//								  {1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1},
//								  {1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,0,1},
//								  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1},
//								  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}}

//=====================================================================

void runDFS();
void DFS(int row, int col, int visited[MAP_HEIGHT][MAP_WIDTH]);

//======================= Path-Finding ================================
//=====================================================================

void runDFS(){			// Base case for DFS
	DFSLevel = 0;
    int visited[MAP_HEIGHT][MAP_WIDTH] = {  [0 ... MAP_HEIGHT-1][0 ... MAP_WIDTH-1] = 0  };    	// manual initialization is needed for the search to work for some reason
	
	DFS(startY, startX, visited);		// run the recursive depth first search
}

void DFS(int row, int col, int visited[MAP_HEIGHT][MAP_WIDTH]){			// Recursive case for DFS
	if(row<0 || row>MAP_HEIGHT-1 || col<0 || col>MAP_WIDTH-1) return;
	if(map[row][col] == 1) return;
	if(visited[row][col] == 1) return;
	if(DFSLevel >= MAX_PATH_LENGTH) return;			// terminate the search upon reaching the path size limit
	
	visited[row][col] = 1;
    
	DFSPath[DFSLevel++] = (Pair) {row, col};		// store the node when first reaching it
	
	DFS(row-1, col, visited);
	if (DFSPath[DFSLevel-1].row!=row || DFSPath[DFSLevel-1].col!=col) { DFSPath[DFSLevel++] = (Pair) {row, col}; }
	DFS(row+1, col, visited);
	if (DFSPath[DFSLevel-1].row!=row || DFSPath[DFSLevel-1].col!=col) { DFSPath[DFSLevel++] = (Pair) {row, col}; }
	DFS(row, col-1, visited);
	if (DFSPath[DFSLevel-1].row!=row || DFSPath[DFSLevel-1].col!=col) { DFSPath[DFSLevel++] = (Pair) {row, col}; }
	DFS(row, col+1, visited);
    
	DFSPath[DFSLevel++] = (Pair) {row, col};		// store the node again when the call terminates
}
