// -- Settings --

#define DEBUG_MODE 0
#define SEARCH_MODE 1				// 0 = DFS, 1 = A*, 2 = IDDFS

#define MAX_FOOD_NO 6

int startX=1, startY=1;				// the robot's starting location