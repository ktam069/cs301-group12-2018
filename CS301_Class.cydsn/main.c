#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <project.h>

#include "defines.h"
#include "vars.h"

#include "usb_code.c"
#include "wheel_controls.c"
#include "path_decisions.c"

#define TOGGLE_LED LED_Write(~LED_Read())

#define FALSE 0
#define TRUE 1

//* ========================================

// Note: Remember to turn off DEBUG_MODE in settings.c

int16 M1_counter = 0;
int16 M2_counter = 0;

int16 result = 0;
uint8 dataReady = 0;

volatile uint8 senseFlags[5] = {0};                 // light sensor flags

volatile uint8 mode = 0;
volatile uint8 modeFlags[4] = {0};

volatile uint8 leftIntersectionDetected = 0;
volatile uint8 rightIntersectionDetected = 0;

volatile uint16 distance_count = 0;
volatile int8 counter = 0;

volatile uint16 robot_speed = 0;

volatile uint16 timer_count = 0;

//* ========================================

void init();

void computePath(int food_no);

void printDFSResults();
void printLevels();
void printGrid();
void printPath(Pair path[]);

//* ========================================

CY_ISR_PROTO(myisr);

CY_ISR(quaddec){                        // Quadrature Decoder ISR checks the counter every 0.5 second
	if (going_straight == 1) {
		M2_counter = QuadDec_M2_GetCounter();
		M1_counter = QuadDec_M1_GetCounter();
		
		distance_count += M2_counter;
		robot_speed = (uint16) (M2_counter / 0.5);          // where 0.5 is the timer period
		
		QuadDec_M1_SetCounter(0);
		QuadDec_M2_SetCounter(0);
		if (M1_counter < M2_counter) {
			speed_up_left();
			slow_down_right();
		} else if (M2_counter < M1_counter) {
			speed_up_right();
			slow_down_left();
		}
	}
	
	Timer_TS_ReadStatusRegister();
}

CY_ISR(timer1){					// 0.2s timer (used for navigating intersections)
	timer_count++;
}

CY_ISR(myisr){ flag_rx = 1; }

CY_ISR(sensor0On)  { senseFlags[0] = 1; } 	// front left sensor
CY_ISR(sensor0Off) { senseFlags[0] = 0; }

CY_ISR(sensor1On)  { senseFlags[1] = 1; } 	// front right sensor
CY_ISR(sensor1Off) { senseFlags[1] = 0; }

CY_ISR(sensor2On)  { senseFlags[2] = 1; } 	// center left sensor
CY_ISR(sensor2Off) { senseFlags[2] = 0; }

CY_ISR(sensor3On)  { senseFlags[3] = 1; } 	// center ref sensor
CY_ISR(sensor3Off) { senseFlags[3] = 0; }

CY_ISR(sensor4On)  { senseFlags[4] = 1; } 	// center right sensor
CY_ISR(sensor4Off) { senseFlags[4] = 0; }

int main() {
    init();         // Initializations of components and ISRs
	stop();
    
    // ------ USB SETUP ----------------    
    #ifdef USE_USB    
        USBUART_Start(0, USBUART_5V_OPERATION);
    #endif
	
	int turnsIndex = 0;		// index for pathVertices (i.e. turns made)
	int food_no = 0;		// target food number from the food list
	
	computePath(food_no);
	
    go_straight();
	
    while(1) {
		
		if ((senseFlags[0] == 1 && senseFlags[1] == 1) || (senseFlags[0] == 0 && senseFlags[1] == 0)) {			// move forward on a straight line or at an intersection
			go_straight();
		} else if (senseFlags[0] == 0 && senseFlags[1] == 1) {			// straighten robot (while on a straight line)
			turn_right();
		} else if (senseFlags[0] == 1 && senseFlags[1] == 0) {			// straighten robot (while on a straight line)
			turn_left();
		}
			
		if ((senseFlags[2] == 1 || senseFlags[4] == 1) && timer_count >= 3) {			// at an intersection - if left or right sensors see a line, and left the last intersection
			stop();
			
			node n = pathVertices[turnsIndex];
			turnsIndex++;
			enum turnDir d = n.turn;									// fetch the next node and get the direction at the vertex
			
			switch (d) {
				case LEFT:
					turn_left_in_place();
					QuadDec_M2_SetCounter(0);
                    QuadDec_M1_SetCounter(0);
					while (senseFlags[0] == 1 || senseFlags[1] == 1) { continue; }		// keep turning while either one of the front sensors detect a line
					while (/*(senseFlags[0] == 0 && senseFlags[1] == 0) ||*/ (QuadDec_M2_GetCounter() < 110)) { continue; }		// keep turning while both of the front sensors don't detect a line
					go_straight();
					QuadDec_M2_SetCounter(0);
					QuadDec_M1_SetCounter(0);
                    timer_count = 0;
					break;
				case RIGHT:
					turn_right_in_place();
                    QuadDec_M2_SetCounter(0);
                    QuadDec_M1_SetCounter(0);
					while (senseFlags[0] == 1 || senseFlags[1] == 1) { continue; }		// keep turning while either one of the front sensors detect a line
					while (/*(senseFlags[0] == 0 && senseFlags[1] == 0) ||*/ (QuadDec_M1_GetCounter() < 110) ){ continue; }		// keep turning while both of the front sensors don't detect a line
					go_straight();
                    QuadDec_M2_SetCounter(0);
					QuadDec_M1_SetCounter(0);
                    timer_count = 0;
					break;
				case BACK:
					turn_right_in_place();
                    QuadDec_M2_SetCounter(0);
                    QuadDec_M1_SetCounter(0);
					while (senseFlags[0] == 1 || senseFlags[1] == 1) { continue; }		// keep turning while either one of the front sensors detect a line
					while ((senseFlags[0] == 0 && senseFlags[1] == 0) || (QuadDec_M1_GetCounter() < 190)) { continue; }		// keep turning while both of the front sensors don't detect a line
					go_straight();
                    QuadDec_M2_SetCounter(0);
                    QuadDec_M1_SetCounter(0);
                    timer_count = 0;
					break;
				case STRAIGHT:
					go_straight();
					timer_count = 0;
					break;
				default:		// shouldn't happen - but may want to do something here
					stop();
			}
			
			if (SEARCH_MODE >= 1) {			// go from the vertex to the pellet
				if (turnsIndex-1==finalShortestPathIndex) {					
					
					//TODO: may need to check the robot's orientation and adjust accordingly if needed
					
					startX = food_list[food_no][0];
					startY = food_list[food_no][1];
					
					uint8 travel_x = abs(n.col - startX);
					uint8 travel_y = abs(n.row - startY);
					
					going_straight = 0;
					
					QuadDec_M2_SetCounter(0);
                    QuadDec_M1_SetCounter(0);
					
					while (QuadDec_M2_GetCounter() < 150*(travel_x + travel_y)) {
						if ((senseFlags[0] == 1 && senseFlags[1] == 1) || (senseFlags[0] == 0 && senseFlags[1] == 0)) {			// move forward on a straight line or at an intersection
							straight_no_quad();
						} else if (senseFlags[0] == 0 && senseFlags[1] == 1) {			// straighten robot (while on a straight line)
							turn_right();
						} else if (senseFlags[0] == 1 && senseFlags[1] == 0) {			// straighten robot (while on a straight line)
							turn_left();
						}
					}	
					
					stop();
					
					timer_count = 0;
					while (timer_count < 6) { continue; }		// stop for a bit
					
					enum dir lastD = lastDirection;
					enum dir newD;
					
					food_no++;

					if (food_no > MAX_FOOD_NO - 1) food_no = 0;

					turnsIndex = 0;
					computePath(food_no);
					
					
					// if we need to turn around before starting the next shortest path
						
					if (SEARCH_MODE == 1) {
						newD = findDirection(aStarPathUsingArrays[0], aStarPathUsingArrays[1]);
					} else if (SEARCH_MODE == 2) {
						newD = findDirection(IDDFSPath[0], IDDFSPath[1]);
					}
					
					if (newD != lastD) {				// turn 180 degrees
						turn_right_in_place();
						QuadDec_M2_SetCounter(0);
						QuadDec_M1_SetCounter(0);
						while (senseFlags[0] == 1 || senseFlags[1] == 1) { continue; }		// keep turning while either one of the front sensors detect a line
						while ((senseFlags[0] == 0 && senseFlags[1] == 0) || (QuadDec_M1_GetCounter() < 190)) { continue; }		// keep turning while both of the front sensors don't detect a line
						go_straight();
						QuadDec_M2_SetCounter(0);
						QuadDec_M1_SetCounter(0);
					}
					
					go_straight();

					continue;				// restart tracking
				}
			}
		} else if (senseFlags[0] == 0 && senseFlags[1] == 0 && senseFlags[2] == 0 && senseFlags[3] == 0 && senseFlags[4] == 0) {		// turn around at dead end
			// (may want to check whether the next turn is actually at a dead end)
			turn_right_in_place();
			while (senseFlags[0] == 1 || senseFlags[1] == 1) { continue; }
			while (senseFlags[0] == 0 && senseFlags[1] == 0) { continue; }
			go_straight();
		}
    }
}


void init(){
    // ----- INITIALIZATIONS ----------
    USBUART_Start(0,USBUART_5V_OPERATION);
    UART_Start();
	
    Timer_TS_Start();
    Timer_TS_1_Start();
    QuadDec_M1_Start();
    QuadDec_M2_Start();
    
    CONTROL_Write(0);
    RF_BT_SELECT_Write(0);
    
    PWM_1_WritePeriod(255);
    PWM_1_Start();
    
    PWM_2_WritePeriod(255);
    PWM_2_Start();
    
    ADC_Start();
    ADC_StartConvert();
    ADC_IRQ_Enable();
    
    distance_count = 0;
    
    QuadDec_M1_SetCounter(0);
    QuadDec_M2_SetCounter(0);
    
    // -- ISR Initialization --
    
    CYGlobalIntEnable;
    isrRF_RX_StartEx(myisr);
    isr_TS_StartEx(quaddec);
    isr_TS_1_StartEx(timer1);
    isr_eoc_StartEx(ADC_ISR);
    
    isr_sense_0On_StartEx(sensor0On);
    isr_sense_0Off_StartEx(sensor0Off);
    
    isr_sense_1On_StartEx(sensor1On);
    isr_sense_1Off_StartEx(sensor1Off);
    
    isr_sense_2On_StartEx(sensor2On);
    isr_sense_2Off_StartEx(sensor2Off);
    
    isr_sense_3On_StartEx(sensor3On);
    isr_sense_3Off_StartEx(sensor3Off);
    
    isr_sense_4On_StartEx(sensor4On);
    isr_sense_4Off_StartEx(sensor4Off);
}

void computePath(int food_no){			// searches (DFS, A*, IDDFS)
	char line1[30];
	if (SEARCH_MODE == 0) {
		runDFS();
		arrayToTurns(DFSPath);
	} else if (SEARCH_MODE == 1) {
		aStarUsingArrays(startY, startX, food_list[food_no][1], food_list[food_no][0]);

		sprintf(line1, "\nstartY:%d startX:%d\n", startY, startX);
		if (DEBUG_MODE) usbPutString(line1);
		sprintf(line1, "\ntargetY:%d targetX:%d\n", food_list[food_no][1], food_list[food_no][0]);
		if (DEBUG_MODE) usbPutString(line1);
		// sprintf(line1, "\nfinished arrayToTurns \n");
		// if (DEBUG_MODE) usbPutString(line1);
		// aStarUsingArrays(startY, startX, food_list[food_no + 1][1], food_list[food_no + 1][0]);
		arrayToTurns(aStarPathUsingArrays);
		sprintf(line1, "\naStarPathUsingArrays[0] row:%d col:%d\n", aStarPathUsingArrays[0].row, aStarPathUsingArrays[0].col);
		if (DEBUG_MODE) usbPutString(line1);
		sprintf(line1, "\naStarPathUsingArrays[1] row:%d col:%d\n", aStarPathUsingArrays[1].row, aStarPathUsingArrays[1].col);
		if (DEBUG_MODE) usbPutString(line1);

		sprintf(line1, "\nfinished arrayToTurns \n");
		if (DEBUG_MODE) usbPutString(line1);
		
	} else if (SEARCH_MODE == 2) {
		searchIDDFS(startY, startX, food_list[food_no][1], food_list[food_no][0]);
		arrayToTurns(IDDFSPath);
	}
}

//* ========================================

// Prints for output of searches

void printPath(Pair path[]){
    int i;
	char line1[30];
	sprintf(line1, "\n PATH: \n___________\n");
	if (DEBUG_MODE) usbPutString(line1);
	for(i = 0; i < MAX_PATH_LENGTH; i++){
		//if(path[i] == NULL) continue;		    // ignore invalid (empty/out of bounds) node
    	sprintf(line1, "\n   step %d: \t(%d,%d)", i, path[i].row, path[i].col);
    	if (DEBUG_MODE) usbPutString(line1);
	}
	sprintf(line1, "\n___________\n");
	if (DEBUG_MODE) usbPutString(line1);
}
