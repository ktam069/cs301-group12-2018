//====================== Searches Program =============================
//=====================================================================

#include <stdio.h>
#include <stdlib.h>

#include "map.h"		// insert the map to be loaded here
#include "settings.h"		// edit settings here
#include "linked_list.c"

//========================== Variables ================================

#define MAP_HEIGHT 15				// dimensions of the map
#define MAP_WIDTH  19

#define MAX_PATH_LENGTH 500			// (some arbitrarily large number, for the longest expected path required)
#define A_STAR_ARRAY_LENGTH 50

typedef struct Pair {
	int row;
	int col;
} Pair;

typedef struct tile {
    int row;
    int col;
    int f;
    int g;
	int parentRow;
	int parentCol;
} tile;

int endX, endY;         // target location

// For DFS
Pair DFSPath[MAX_PATH_LENGTH] = {{-1, -1}};		// stores every grid of the DFS; note that the path continues after reaching the last node (until it returns to the start)
int DFSLevel = 0;					// keeps track of the node traversal order (which is used as the index for the DSFPath array)

// For A* Linked List
Pair aStarPath[] = {{-1, -1}};		// stores every grid of the a*; note that the path continues after reaching the last node (until it returns to the start)

// For A* using arrays
Pair aStarPathUsingArrays[MAX_PATH_LENGTH] = {{-1, -1}};
tile openListUsingArrays[A_STAR_ARRAY_LENGTH] = {{-1, -1, -1, -1, -1}};
tile closedListUsingArrays[A_STAR_ARRAY_LENGTH] = {{-1, -1, -1, -1, -1}};
tile pathListUsingArrays[A_STAR_ARRAY_LENGTH] = {{-1, -1, -1, -1, -1}};
tile helperArray[A_STAR_ARRAY_LENGTH] = {{-1, -1, -1, -1, -1}};

// Helper functions for A* using arrays
void add(char c, tile t);			// where char c specifies which array (list) to act on
void removeTileFromOpen(int index);
int getIndexOfTileWithSmallestFInOpenList();
uint8 openListIsEmpty();
void initialiseStructArrays();
void refineAStarFinalPath();

// Helper variable for A* using arrays
int indexForOpenList = 0;
int indexForClosedList = 0;
int indexForPath = 0;


// For IDDFS
Pair IDDFSPath[MAX_PATH_LENGTH] = {{-1, -1}};

//=====================================================================

void runDFS();
void DFS(int row, int col, int visited[MAP_HEIGHT][MAP_WIDTH]);

// A* using arrays
void aStarUsingArrays(int startRow, int startCol, int targetRow, int targetCol);
uint8 hasTileWithSamePositionAndLowerF(char c, tile t);
int calculateH(int currentRow, int currentCol, int targetRow, int targetCol);


//=========================== DFS =====================================
//=====================================================================

void runDFS(){			// Base case for DFS
	DFSLevel = 0;
    int visited[MAP_HEIGHT][MAP_WIDTH] = {  [0 ... MAP_HEIGHT-1][0 ... MAP_WIDTH-1] = 0  };    	// manual initialization is needed for the search to work for some reason
	
	DFS(startY, startX, visited);		// run the recursive depth first search
}

void DFS(int row, int col, int visited[MAP_HEIGHT][MAP_WIDTH]){			// Recursive case for DFS
	if(row<0 || row>MAP_HEIGHT-1 || col<0 || col>MAP_WIDTH-1) return;
	if(map[row][col] == 1) return;
	if(visited[row][col] == 1) return;
	if(DFSLevel >= MAX_PATH_LENGTH) return;			// terminate the search upon reaching the path size limit
	
	visited[row][col] = 1;
    
	DFSPath[DFSLevel++] = (Pair) {row, col};		// store the node when first reaching it
	
	DFS(row-1, col, visited);
	if (DFSPath[DFSLevel-1].row!=row || DFSPath[DFSLevel-1].col!=col) { DFSPath[DFSLevel++] = (Pair) {row, col}; }		// (store any new node as well - for nodes visited 2+ times)
	DFS(row+1, col, visited);
	if (DFSPath[DFSLevel-1].row!=row || DFSPath[DFSLevel-1].col!=col) { DFSPath[DFSLevel++] = (Pair) {row, col}; }
	DFS(row, col-1, visited);
	if (DFSPath[DFSLevel-1].row!=row || DFSPath[DFSLevel-1].col!=col) { DFSPath[DFSLevel++] = (Pair) {row, col}; }
	DFS(row, col+1, visited);
    
	DFSPath[DFSLevel++] = (Pair) {row, col};		// store the node again when the call terminates
}

//============================ A* arrays ==============================
//=====================================================================

void aStarUsingArrays(int startRow, int startCol, int targetRow, int targetCol) {
	endX = targetCol;
	endY = targetRow;

	initialiseStructArrays();
	
	tile start = {startRow, startCol, 0, 0, -1, -1};
	add('o', start);
	
	while(!openListIsEmpty()) {
		int index = getIndexOfTileWithSmallestFInOpenList();
		tile tileWithSmallestF = openListUsingArrays[index];
		removeTileFromOpen(index);
		
		// North
		if (tileWithSmallestF.row > 1 && map[tileWithSmallestF.row - 1][tileWithSmallestF.col] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row - 1, tileWithSmallestF.col, targetRow, targetCol) + g;
			tile north = {tileWithSmallestF.row - 1, tileWithSmallestF.col, f, g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (north.row == targetRow && north.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = north;
				indexForPath++;
				
				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', north) && !hasTileWithSamePositionAndLowerF('c', north)) {
					add('o', north);
				}
			}
		}
		
		// East
		if (tileWithSmallestF.col < MAP_WIDTH && map[tileWithSmallestF.row][tileWithSmallestF.col + 1] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row, tileWithSmallestF.col + 1, targetRow, targetCol) + g;
			tile east = {tileWithSmallestF.row, tileWithSmallestF.col + 1, f, g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (east.row == targetRow && east.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = east;
				indexForPath++;

				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', east) && !hasTileWithSamePositionAndLowerF('c', east)) {
					add('o', east);
				}
			}
		}
		
		// South
		if (tileWithSmallestF.row < MAP_HEIGHT && map[tileWithSmallestF.row + 1][tileWithSmallestF.col] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row + 1, tileWithSmallestF.col, targetRow, targetCol) + g;
			tile south = {tileWithSmallestF.row + 1, tileWithSmallestF.col, f,g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (south.row == targetRow && south.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = south;
				indexForPath++;

				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', south) && !hasTileWithSamePositionAndLowerF('c', south)) {
					add('o', south);
				}
			}
		}
		
		// West
		if (tileWithSmallestF.col > 1 && map[tileWithSmallestF.row][tileWithSmallestF.col - 1] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row, tileWithSmallestF.col - 1, targetRow, targetCol) + g;
			tile west = {tileWithSmallestF.row, tileWithSmallestF.col - 1, f,g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (west.row == targetRow && west.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = west;
				indexForPath++;	
				
				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', west) && !hasTileWithSamePositionAndLowerF('c', west)) {
					add('o', west);
				}
			}
		}
		
		add('c', tileWithSmallestF);
		pathListUsingArrays[indexForPath] = tileWithSmallestF;
		indexForPath++;
	}

	refineAStarFinalPath();
}

void refineAStarFinalPath() {
	int j;
	int indexOfHelperArray = 1;
	int i = indexForPath - 1;
	helperArray[0] = pathListUsingArrays[i];
	while (i > 0) {
		j = i - 1;
		while (!(pathListUsingArrays[i].parentRow == pathListUsingArrays[j].row && pathListUsingArrays[i].parentCol == pathListUsingArrays[j].col)) {
			j = j - 1;
		}
		helperArray[indexOfHelperArray] = pathListUsingArrays[j];
		indexOfHelperArray++;
		i = j;
	}

	int n = 0;
	int m;
	for (m = indexOfHelperArray; m > 0; m--) {
		aStarPathUsingArrays[n] = (Pair) {helperArray[m - 1].row, helperArray[m - 1].col};

		n++;
	}
	return;
}

void initialiseStructArrays() {
	indexForOpenList = 0;
	indexForClosedList = 0;
	indexForPath = 0;
	int i;
	for (i = 0; i < A_STAR_ARRAY_LENGTH; i++) {
		openListUsingArrays[i] = (tile) {-1, -1, -1, -1, -1, -1};
		closedListUsingArrays[i] = (tile) {-1, -1, -1, -1, -1, -1};
		pathListUsingArrays[i] = (tile) {-1, -1, -1, -1, -1, -1};
		helperArray[i] = (tile) {-1, -1, -1, -1, -1, -1};
	}
	for (i = 0; i < MAX_PATH_LENGTH; i++) {
		aStarPathUsingArrays[i] = (Pair) {-1, -1};
	}
}
	
uint8 hasTileWithSamePositionAndLowerF(char c, tile t) {
	uint8 hasTileWithSamePositionAndLowerF = 0;
	int i = 0;
	
	if (c == 'o') {
		while(openListUsingArrays[i].row != -1) {
			if (openListUsingArrays[i].row == t.row && openListUsingArrays[i].col == t.col && openListUsingArrays[i].f <= t.f) {
				hasTileWithSamePositionAndLowerF = 1;
			}
			i++;
		}
	} else if (c == 'c') {
		while(closedListUsingArrays[i].row != -1) {
			if (closedListUsingArrays[i].row == t.row && closedListUsingArrays[i].col == t.col && closedListUsingArrays[i].f <= t.f) {
				hasTileWithSamePositionAndLowerF = 1;
			}
			i++;
		}
	}
	
	return hasTileWithSamePositionAndLowerF;
}	

void add(char c, tile t) {			// where char c specifies which array (list) to act on
	if (c == 'o') {
		openListUsingArrays[indexForOpenList] = t;
		indexForOpenList++;
	} else if (c == 'c') {
		closedListUsingArrays[indexForClosedList] = t;
		indexForClosedList++;
	}
}

void removeTileFromOpen(int index) {		// where char c specifies which array (list) to act on
	int i = index + 1;
	
	while(openListUsingArrays[i].row != -1) {
		openListUsingArrays[i - 1] = openListUsingArrays[i];
		i++;
	}
	openListUsingArrays[i - 1].row = -1;
	openListUsingArrays[i - 1].col = -1;
	openListUsingArrays[i - 1].f = -1;
	openListUsingArrays[i - 1].g = -1;
	openListUsingArrays[i - 1].parentRow = -1;
	openListUsingArrays[i - 1].parentCol = -1;
	
	indexForOpenList--;
}

int getIndexOfTileWithSmallestFInOpenList() {
	int indexOfSmallestF = 0;
	int i = 1;
	
	while(openListUsingArrays[i].row != -1) {
		if (openListUsingArrays[i].f < openListUsingArrays[indexOfSmallestF].f) {
			indexOfSmallestF = i;
		}
		i++;
	}
	
	return indexOfSmallestF;
}

uint8 openListIsEmpty() {	// where char c specifies which array (list) to act on
	if (openListUsingArrays[0].row == -1) {
		return 1;
	} else {
		return 0;
	}
}

int calculateH(int currentRow, int currentCol, int targetRow, int targetCol) {
    return abs(currentRow - targetRow) + abs(currentCol - targetCol);
}

//========================== IDDFS ====================================
//=====================================================================

// IDDFS - iterative deepening depth first search (i.e. BFS implemented using repeated DFS)

// Note that IDDFS becomes quite slow past a depth of 16 or so (still works though)

int depthLimit = 64;		// maximum depth to search before stopping (for IDDFS / traceback)



uint8 searchIDDFS(int startY, int startX, int targetY, int targetX);
uint8 DLS(int row, int col, int depth, int level, int lvls[MAP_HEIGHT][MAP_WIDTH]);

void reconstructPath(int lvls[MAP_HEIGHT][MAP_WIDTH]);
void recursiveGetPath(int row, int col, int lastLevel, int lvls[MAP_HEIGHT][MAP_WIDTH]);


uint8 searchIDDFS(int startY, int startX, int targetY, int targetX){
	char line1[30];
	sprintf(line1, "inside IDDFS");
    if (DEBUG_MODE) usbPutString(line1);
	
	endX = targetX;
	endY = targetY;
	
	sprintf(line1, "set targets");
    if (DEBUG_MODE) usbPutString(line1);
	
	int levels[MAP_HEIGHT][MAP_WIDTH] = { [0 ... MAP_HEIGHT-1][0 ... MAP_WIDTH-1] = -1 };             // stores the order the nodes were traversed in (to construct the path to be taken)
	
	sprintf(line1, "initialised arrays");
    if (DEBUG_MODE) usbPutString(line1);
	
	int depth;
	for (depth = 0; depth < depthLimit; depth++) {
		sprintf(line1, "depth:%d", depth);
			if (DEBUG_MODE) usbPutString(line1);
			
		uint8 targetFound = DLS(startY, startX, depth, depth-1, levels);
		reconstructPath(levels);
		if (targetFound) {
			sprintf(line1, "exiting searchIDDFS");
			if (DEBUG_MODE) usbPutString(line1);
			return 1;
		}
	}
	
	sprintf(line1, "exiting searchIDDFS");
    if (DEBUG_MODE) usbPutString(line1);
	return 0;
}

uint8 DLS(int row, int col, int depth, int level, int lvls[MAP_HEIGHT][MAP_WIDTH]){		// only depth matters - does not know about visited nodes
	if (row==endY && col==endX){ lvls[row][col] = level+1; return 1; }	// target reached

	if (depth <= 0) return 0;                                            	// end of search reached for this iteration
	if (row<0 || row>MAP_HEIGHT-1 || col<0 || col>MAP_WIDTH-1) return 0;	// outside of valid range
	if (map[row][col] == 1) return 0;                                 		// is a wall - invalid
	
	if (lvls[row][col] == -1) lvls[row][col] = level;        			// record the level of this grid square
	
	uint8 targetFound0 = DLS(row-1, col, depth-1, level, lvls);
	uint8 targetFound1 = DLS(row+1, col, depth-1, level, lvls);
	uint8 targetFound2 = DLS(row, col-1, depth-1, level, lvls);
	uint8 targetFound3 = DLS(row, col+1, depth-1, level, lvls);

	return (targetFound0 || targetFound1 || targetFound2 || targetFound3);
}

//==================== Path Reconstruction ============================
//=====================================================================

// Using the 'levels' array to create the path (going from the highest level down to 0)

// To reconstruct the path that should be taken...
// ... 1) Repeatedly find the next node from adjacent squares.
// ... 2) If it can't be found, then backtrack until the next node is found.
// ... 3) Repeat 1 and 2 until the whole map is traversed. (i.e. level==0)

void reconstructPath(int lvls[MAP_HEIGHT][MAP_WIDTH]){
	// builds the path from highest level down to lowest level, with the index in the array being the level of the grid
	recursiveGetPath(endY, endX, MAX_PATH_LENGTH+1, lvls);				// MAX_PATH_LENGTH+1 can be replaced with any arbitrarily large value
}

void recursiveGetPath(int row, int col, int lastLevel, int lvls[MAP_HEIGHT][MAP_WIDTH]){		// note that level is also the index for path[]
	int level = lvls[row][col];
	if (row==startY && col==startX) IDDFSPath[level] = (Pair) {row, col};		// extra condition if levels aren't -1
	if (level <= 0) return;					// no data for this grid (i.e. wall, so skip)
	if (level >= lastLevel) return;			// wrong direction (level not decreasing)
	
    /* TODO: NEED TO FIGURE OUT HOW TO DO NULL CHECKING */
    //if(path[level] != NULL) return;		// no need to change already created path
	
	// valid direction - store this grid
	IDDFSPath[level] = (Pair) {row, col};
	
	recursiveGetPath(row-1, col, level, lvls);
	recursiveGetPath(row+1, col, level, lvls);
	recursiveGetPath(row, col-1, level, lvls);
	recursiveGetPath(row, col+1, level, lvls);
}