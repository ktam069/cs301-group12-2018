#include <stdio.h>
#include <stdlib.h>

enum turnDir {STRAIGHT, LEFT, RIGHT, BACK};			// relative direction that the robot should turn in at a node

typedef struct node {
    int row;
    int col;
    int f;
	enum turnDir turn;
    struct node* next;
} node;

node* create(int row, int col, int f, enum turnDir turn, node* next);
node* append(node* head, node* n);
node* remove_front(node* head);
node* remove_back(node* head);
node* remove_any(node* head, node* n);
uint8 isEmpty(node* head);
node* get_node_with_smallest_f(node* head);

node* create(int row, int col, int f, enum turnDir turn, node* next){
    node* new_node = (node*)malloc(sizeof(node));
    if(new_node == NULL) {
        printf("Error creating a new node.\n");
        exit(0);
    }
    new_node->row = row;
	new_node->col = col;
    new_node->next = next;
    new_node->turn = turn;
 
    return new_node;
}

node* append(node* head, node* n){
	char line1[30];
	sprintf(line1, "\n ____________________in append function");
	usbPutString(line1);
	
    if (head == NULL) {
        head = n;
		
		sprintf(line1, "\n ____________________first node appended ");
		usbPutString(line1);
    } else {
		sprintf(line1, "\n ____________________subsequent node to be appended ");
		usbPutString(line1);
		
        node* cursor = head;
        while (cursor->next != NULL) {
            cursor = cursor->next;
		}
        cursor->next = n;
		
		sprintf(line1, "\n ____________________subsequent node appended");
		usbPutString(line1);
    }
	sprintf(line1, "\n ____________________return to calling function");
	usbPutString(line1);
    return head;
}

node* remove_front(node* head){
    if(head == NULL)
        return NULL;

    node *front = head;
    head = head->next;
    front->next = NULL;

    if(front == head)
        head = NULL;

    free(front);

    return head;
}

node* remove_back(node* head){
    if(head == NULL)
        return NULL;
 
    node *cursor = head;
    node *back = NULL;

    while(cursor->next != NULL){
        back = cursor;
        cursor = cursor->next;
    }

    if(back != NULL)
        back->next = NULL;
 
    if(cursor == head)
        head = NULL;
 
    free(cursor);
 
    return head;
}

node* remove_any(node* head, node* n){
    if(n == head){
        head = remove_front(head);
        return head;
    }
 
    if(n->next == NULL) {
        head = remove_back(head);
        return head;
    }
 
    node* cursor = head;
    while(cursor != NULL) {
        if(cursor->next == n) break;
        cursor = cursor->next;
    }
 
    if(cursor != NULL) {
        node* tmp = cursor->next;
        cursor->next = tmp->next;
        tmp->next = NULL;
        free(tmp);
    }
    return head;
}

uint8 isEmptyLinkedList(node* head) {
    if (head == NULL) {
        return 1;
    } 
    
    return 0;
}

node* get_node_with_smallest_f(node* head) {
    node *cursor = head;
    node *nodeWithSmallestF = head;

    while(cursor != NULL) {
        if (cursor->f < nodeWithSmallestF->f) {
            nodeWithSmallestF = cursor;
        }
        cursor = cursor->next;
    }

    return nodeWithSmallestF;
}

void printList(node* head) { 
	node *cursor = head;

	char line1[30];	
	sprintf(line1, "printing list\n"); 
	usbPutString(line1);
	
	while (cursor != NULL) {
		sprintf(line1, "row: %d ", cursor->row); 
		usbPutString(line1);
		sprintf(line1, "col: %d \n", cursor->col); 
		usbPutString(line1);
	
		cursor = cursor->next; 
	}
	
	free(cursor);
} 