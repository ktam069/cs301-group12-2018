#include <stdio.h>
#include <stdlib.h>

#include "map.h"		// insert the map to be loaded here
//========================== Variables ================================

#define MAP_HEIGHT 15				// dimensions of the map
#define MAP_WIDTH  19

#define A_STAR_ARRAY_LENGTH 50

typedef struct Pair {
	int row;
	int col;
} Pair;

typedef struct tile {
    int row;
    int col;
    int f;
    int g;
	int parentRow;
	int parentCol;
} tile;

// int endX, endY;         // target location

// For A* using arrays
Pair aStarPathUsingArrays[A_STAR_ARRAY_LENGTH];
tile openListUsingArrays[A_STAR_ARRAY_LENGTH];
tile closedListUsingArrays[A_STAR_ARRAY_LENGTH];
tile pathListUsingArrays[A_STAR_ARRAY_LENGTH];
tile helperArray[A_STAR_ARRAY_LENGTH];

// Helper functions for A* using arrays
void add(char c, tile t);			// where char c specifies which array (list) to act on
void removeTileFromOpen(int index);
int getIndexOfTileWithSmallestFInOpenList();
uint openListIsEmpty();
void initialiseStructArrays();
void refineAStarFinalPath();

// Helper variable for A* using arrays
int indexForOpenList = 0;
int indexForClosedList = 0;
int indexForPath = 0;

//=====================================================================

void aStarUsingArrays(int startRow, int startCol, int targetRow, int targetCol);
uint hasTileWithSamePositionAndLowerF(char c, tile t);
uint hasTileWithSamePositionAndLowerG(char c, tile t);
uint hasTileWithSamePosition(char c, tile t);
int calculateH(int currentRow, int currentCol, int targetRow, int targetCol);

//=====================================================================

int main() {
	printf("\nEntering test for A* algorithm");

	int startRow = 7;
	int startCol = 1;
	int targetRow = 11;
	int targetCol = 5;
	aStarUsingArrays(startRow, startCol, targetRow, targetCol);

	printf("\nExiting test for A* algorithm");
	return(0);
}

//=====================================================================

void aStarUsingArrays(int startRow, int startCol, int targetRow, int targetCol) {
	// endX = targetCol;
	// endY = targetRow;

	initialiseStructArrays();
	
	tile start = {startRow, startCol, 0, 0, -1, -1};
	add('o', start);
	
	while(!openListIsEmpty()) {
		int index = getIndexOfTileWithSmallestFInOpenList();
		tile tileWithSmallestF = openListUsingArrays[index];
		removeTileFromOpen(index);
		
		// North
		if (tileWithSmallestF.row > 1 && map[tileWithSmallestF.row - 1][tileWithSmallestF.col] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row - 1, tileWithSmallestF.col, targetRow, targetCol) + g;
			tile north = {tileWithSmallestF.row - 1, tileWithSmallestF.col, f, g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (north.row == targetRow && north.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = north;
				indexForPath++;
				
				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', north) && !hasTileWithSamePositionAndLowerF('c', north)) {
					add('o', north);
				}
			}
		}
		
		// East
		if (tileWithSmallestF.col < MAP_WIDTH && map[tileWithSmallestF.row][tileWithSmallestF.col + 1] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row, tileWithSmallestF.col + 1, targetRow, targetCol) + g;
			tile east = {tileWithSmallestF.row, tileWithSmallestF.col + 1, f, g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (east.row == targetRow && east.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = east;
				indexForPath++;

				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', east) && !hasTileWithSamePositionAndLowerF('c', east)) {
					add('o', east);
				}
			}
		}
		
		// South
		if (tileWithSmallestF.row < MAP_HEIGHT && map[tileWithSmallestF.row + 1][tileWithSmallestF.col] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row + 1, tileWithSmallestF.col, targetRow, targetCol) + g;
			tile south = {tileWithSmallestF.row + 1, tileWithSmallestF.col, f,g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (south.row == targetRow && south.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = south;
				indexForPath++;

				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', south) && !hasTileWithSamePositionAndLowerF('c', south)) {
					add('o', south);
				}
			}
		}
		
		// West
		if (tileWithSmallestF.col > 1 && map[tileWithSmallestF.row][tileWithSmallestF.col - 1] == 0) {
			int g = tileWithSmallestF.g + 1;	
			int f = calculateH(tileWithSmallestF.row, tileWithSmallestF.col - 1, targetRow, targetCol) + g;
			tile west = {tileWithSmallestF.row, tileWithSmallestF.col - 1, f,g, tileWithSmallestF.row, tileWithSmallestF.col};
			
			if (west.row == targetRow && west.col == targetCol) {
				pathListUsingArrays[indexForPath] = tileWithSmallestF;
				indexForPath++;
				pathListUsingArrays[indexForPath] = west;
				indexForPath++;	
				
				refineAStarFinalPath();

				return;
			} else {
				if (!hasTileWithSamePositionAndLowerF('o', west) && !hasTileWithSamePositionAndLowerF('c', west)) {
					add('o', west);
				}
			}
		}
		
		add('c', tileWithSmallestF);
		pathListUsingArrays[indexForPath] = tileWithSmallestF;
		indexForPath++;
	}

	refineAStarFinalPath();
}

void refineAStarFinalPath() {
	int j;
	int indexOfHelperArray = 1;
	int i = indexForPath - 1;
	helperArray[0] = pathListUsingArrays[i];

	while (i > 0) {
		j = i - 1;
		while (!(pathListUsingArrays[i].parentRow == pathListUsingArrays[j].row && pathListUsingArrays[i].parentCol == pathListUsingArrays[j].col)) {
			j = j - 1;
		}
		helperArray[indexOfHelperArray] = pathListUsingArrays[j];
		indexOfHelperArray++;
		i = j;
	}

	int n = 0;
	int m;
	for (m = indexOfHelperArray; m > 0; m--) {
		aStarPathUsingArrays[n] = (Pair) {helperArray[m - 1].row, helperArray[m - 1].col};
		printf("\naStarPathUsingArrays[%d] row: %d col: %d", n, helperArray[m - 1].row, helperArray[m - 1].col);

		n++;
	}
}

void initialiseStructArrays() {
	indexForOpenList = 0;
	indexForClosedList = 0;
	indexForPath = 0;

	int i;
	for (i = 0; i < A_STAR_ARRAY_LENGTH; i++) {
		openListUsingArrays[i] = (tile) {-1, -1, -1, -1, -1, -1};
		closedListUsingArrays[i] = (tile) {-1, -1, -1, -1, -1, -1};
		pathListUsingArrays[i] = (tile) {-1, -1, -1, -1, -1, -1};
		helperArray[i] = (tile) {-1, -1, -1, -1, -1, -1};
		aStarPathUsingArrays[i] = (Pair) {-1, -1};
	}
}
	
uint hasTileWithSamePositionAndLowerF(char c, tile t) {
	uint hasTileWithSamePositionAndLowerF = 0;
	int i = 0;
	
	if (c == 'o') {
		while(openListUsingArrays[i].row != -1) {
			if (openListUsingArrays[i].row == t.row && openListUsingArrays[i].col == t.col && openListUsingArrays[i].f <= t.f) {
				hasTileWithSamePositionAndLowerF = 1;
			}
			i++;
		}
	} else if (c == 'c') {
		while(closedListUsingArrays[i].row != -1) {
			if (closedListUsingArrays[i].row == t.row && closedListUsingArrays[i].col == t.col && closedListUsingArrays[i].f <= t.f) {
				hasTileWithSamePositionAndLowerF = 1;
			}
			i++;
		}
	}
	
	return hasTileWithSamePositionAndLowerF;
}	

uint hasTileWithSamePositionAndLowerG(char c, tile t) {
	uint hasTileWithSamePositionAndLowerG = 0;
	int i = 0;
	
	if (c == 'o') {
		while(openListUsingArrays[i].row != -1) {
			if (openListUsingArrays[i].row == t.row && openListUsingArrays[i].col == t.col && openListUsingArrays[i].g <= t.g) {
				hasTileWithSamePositionAndLowerG = 1;
			}
			i++;
		}
	} else if (c == 'c') {
		while(closedListUsingArrays[i].row != -1) {
			if (closedListUsingArrays[i].row == t.row && closedListUsingArrays[i].col == t.col && closedListUsingArrays[i].g <= t.g) {
				hasTileWithSamePositionAndLowerG = 1;
			}
			i++;
		}
	}
	
	return hasTileWithSamePositionAndLowerG;
}	

uint hasTileWithSamePosition(char c, tile t) {
	uint hasTileWithSamePosition = 0;
	int i = 0;
	
	if (c == 'o') {
		while(openListUsingArrays[i].row != -1) {
			if (openListUsingArrays[i].row == t.row && openListUsingArrays[i].col == t.col) {
				hasTileWithSamePosition = 1;
			}
			i++;
		}
	} else if (c == 'c') {
		while(closedListUsingArrays[i].row != -1) {
			if (closedListUsingArrays[i].row == t.row && closedListUsingArrays[i].col == t.col) {
				hasTileWithSamePosition = 1;
			}
			i++;
		}
	}
	
	return hasTileWithSamePosition;
}	

void add(char c, tile t) {			// where char c specifies which array (list) to act on
	if (c == 'o') {
		openListUsingArrays[indexForOpenList] = t;
		indexForOpenList++;
	} else if (c == 'c') {
		closedListUsingArrays[indexForClosedList] = t;
		indexForClosedList++;
	}
}

void removeTileFromOpen(int index) {		// where char c specifies which array (list) to act on
	int i = index + 1;
	
	while(openListUsingArrays[i].row != -1) {
		openListUsingArrays[i - 1] = openListUsingArrays[i];
		i++;
	}
	openListUsingArrays[i - 1].row = -1;
	openListUsingArrays[i - 1].col = -1;
	openListUsingArrays[i - 1].f = -1;
	openListUsingArrays[i - 1].g = -1;
	openListUsingArrays[i - 1].parentRow = -1;
	openListUsingArrays[i - 1].parentCol = -1;
	
	indexForOpenList--;
}

int getIndexOfTileWithSmallestFInOpenList() {
	int indexOfSmallestF = 0;
	int i = 1;
	
	while(openListUsingArrays[i].row != -1) {
		if (openListUsingArrays[i].f < openListUsingArrays[indexOfSmallestF].f) {
			indexOfSmallestF = i;
		}
		i++;
	}
	
	return indexOfSmallestF;
}

uint openListIsEmpty() {	// where char c specifies which array (list) to act on
	if (openListUsingArrays[0].row == -1) {
		return 1;
	} else {
		return 0;
	}
}

int calculateH(int currentRow, int currentCol, int targetRow, int targetCol) {
    return abs(currentRow - targetRow) + abs(currentCol - targetCol);
}
